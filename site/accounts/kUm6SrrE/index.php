<?php

return [
    'email' => 'pauline.gourlet@gmail.com',
    'language' => 'fr',
    'name' => 'Pauline Gourlet',
    'role' => 'admin'
];