<?php

return [
    'email' => 'sylvia.fredriksson@gmail.com',
    'language' => 'fr',
    'name' => 'Sylvia Fredriksson',
    'role' => 'admin'
];