<?php

return [
    'email' => 'nicolas.loubet@protonmail.com',
    'language' => 'fr',
    'name' => 'Nicolas Loubet',
    'role' => 'admin'
];