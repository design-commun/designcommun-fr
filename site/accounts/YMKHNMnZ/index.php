<?php

return [
    'email' => 'rigot.elise@gmail.com',
    'language' => 'fr',
    'name' => 'Élise Rigot',
    'role' => 'admin'
];