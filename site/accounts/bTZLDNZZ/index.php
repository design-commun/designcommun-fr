<?php

return [
    'email' => 'lysiane.lagadic@gmail.com',
    'language' => 'fr',
    'name' => 'Lysiane Lagadic',
    'role' => 'admin'
];