<?php

return [
    'email' => 'g.barbareau@gmail.com',
    'language' => 'fr',
    'name' => 'Guillaume Barbareau',
    'role' => 'admin'
];