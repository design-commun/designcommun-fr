<?php

return [
    'email' => 'mikhael.pommier@protonmail.com',
    'language' => 'fr',
    'name' => 'Mikhaël Pommier',
    'role' => 'admin'
];