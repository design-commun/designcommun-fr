<?php

return [
    'email' => 'thomas@collectifbam.fr',
    'language' => 'fr',
    'name' => 'Thomas Thibault',
    'role' => 'admin'
];