<?php

return [
    'email' => 'estelle@design-friction.com',
    'language' => 'fr',
    'name' => 'Estelle Hary',
    'role' => 'admin'
];