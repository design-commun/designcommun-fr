<?php

return [
    'email' => 'js@ousontlesdragons.fr',
    'language' => 'fr',
    'name' => 'Joachim Savin',
    'role' => 'admin'
];