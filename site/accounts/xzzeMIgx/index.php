<?php

return [
    'email' => 'timothee@goguely.com',
    'language' => 'fr',
    'name' => 'Timothée Goguely',
    'role' => 'admin'
];