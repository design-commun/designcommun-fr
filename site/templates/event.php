<?php snippet('header') ?>

  <div class="container min-h-full flex flex-col lg:flex-row">
    
    <header id="header" class="w-full lg:w-12 min-h-14">
      <nav role="navigation" class="flex justify-between">
        <a href="<?= $site->homePage()->url() ?>" title="Retour"><span class="hidden lg:inline lg:fixed">←</span><span class="lg:hidden"><?= $site->title() ?></span></a>
      </nav>
    </header>
    
    <div class="relative flex-1 flex flex-col">

      <main class="flex-grow p-4 pb-16 text-color-30-dark" itemscope itemtype="http://schema.org/Event">
        
        <nav id="breadcrumb" class="text-base m-0 flex justify-between">
          <ol class="flex">
            <li><a href="<?= $page->parent()->url() ?>"><?= $page->parent()->title()->lower() ?></a></li>
            <li class="opacity-75"><?= $page->title()->lower() ?></li>
          </ol>
          <?php snippet('svg/calendar') ?>
        </nav>
          
        <header class="flex flex-col">
          <h1 class="m-0 text-5xl" itemprop="name"><?= $page->title() ?></h1>
          <p class="item__date order-first mb-2">
            <?php if ($page->endDate()->isNotEmpty()): ?>
            <time datetime="<?= $page->startDate() ?>"><?= strftime('%e', strtotime($page->startDate())) ?></time><span aria-label="au">→</span><time datetime="<?= $page->endDate() ?>"><?= strftime('%e %B %Y', strtotime($page->endDate())) ?></time>
            <meta itemprop="startDate" content="<?= $page->startDate() ?>" />
            <meta itemprop="endDate" content="<?= $page->endDate() ?>" />
            <?php else: ?>
            <time datetime="<?= $page->startDate() ?>"><?= strftime('%e %B %Y', strtotime($page->startDate())) ?></time>
            <meta itemprop="startDate" content="<?= $page->startDate() ?>" />
            <?php endif ?>
          </p>

          <?php if ($page->link()->isNotEmpty()): ?>
          <p class="mono" aria-label="Lien"><a href="<?= $page->link() ?>" itemprop="url"><?= preg_replace( "#^[^:/.]*[:/]+#i", "", preg_replace( "{/$}", "", urldecode($page->link()) ) ) ?></a></p>
          <?php endif ?>

          <?php if ($page->locality()->isNotEmpty()): ?>
          <p class="mono" aria-label="Lieu" itemprop="location" itemscope itemtype="http://schema.org/Place">
            <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
              <span itemprop="addressLocality"><?= $page->locality() ?></span>, 
              <?php if ($page->department()->isNotEmpty()): ?>
              <span><?= $page->department() ?></span><?php endif ?>
              <?php if ($page->region()->isNotEmpty()): ?>
              <meta itemprop="addressRegion" content="<?= $page->region() ?>" /><?php endif ?>
              <?php if ($page->postalCode()->isNotEmpty()): ?>
              <meta itemprop="postalCode" content="<?= $page->postalCode() ?>" /><?php endif ?>
              <?php if ($page->country()->isNotEmpty()): ?>
              <meta itemprop="addressCountry" content="<?= $page->country() ?>" /><?php endif ?>
            </span>
            <meta itemprop="name" content="<?= $page->locality() ?>">
          </p>
          <?php endif ?>
        </header>

        <div class="max-w-3xl markdown">
          <p class="text-2xl" itemprop="description"><?= $page->text()->kti() ?></p>
          
          <?php if ($page->hasAudio()): ?>
          <?php $audio = $page->audio()->first() ?>
          <figure class="audio mt-16">
            <figcaption>Restitution audio réalisée par Élise Rigot <small class="opacity-75">(<?= $audio->niceSize() ?>)</small></figcaption>
            <audio src="<?= $audio->url() ?>" controls preload="metadata">
              Votre navigateur ne supporte pas la balise <code>audio</code>.
            </audio>
          </figure>
          <?php endif ?>

          <div class="mt-16"><?= $page->restitution()->kt() ?></div>
        </div>

      </main>

      <?php snippet('footer-page') ?>

    </div>
  </div>

  <?php snippet('footer') ?>
