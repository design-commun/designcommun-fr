<?php snippet('header') ?>

	<div class="container min-h-full flex flex-col lg:flex-row">
		
		<header id="header" class="w-full lg:w-12 min-h-14">
			<nav role="navigation" class="flex justify-between">
				<a href="<?= $site->homePage()->url() ?>" title="Retour"><span class="hidden lg:inline lg:fixed">←</span><span class="lg:hidden"><?= $site->title() ?></span></a>
			</nav>
		</header>
		
		<div class="relative flex-1 flex flex-col">
			<main class="flex-grow text-color-30-dark">
				
				<div class="sections">
					<section class="events section flex-1">
						<header class="flex justify-between">
							<h1 class="text-base m-0"><?= $page->title()->lower() ?></h1>
							<?php snippet('svg/calendar') ?>
						</header>

						<p class="text-2xl max-w-3xl mb-14"><?= $page->text()->kti() ?></p>
						
						<?php foreach ($page->children()->listed()->flip() as $event): ?>
						<article class="item event mb-14" itemscope itemtype="http://schema.org/Event">
							<header class="flex flex-col">
								<h2 class="m-0" itemprop="name"><a href="<?= $event->url() ?>" class="border-b"><?= $event->title() ?></a></h2>
								<p class="item__date order-first">
									<?php if ($event->endDate()->isNotEmpty()): ?>
									<time datetime="<?= $event->startDate() ?>"><?= strftime('%e', strtotime($event->startDate())) ?></time><span aria-label="au">→</span><time datetime="<?= $event->endDate() ?>"><?= strftime('%e %B %Y', strtotime($event->endDate())) ?></time>
									<meta itemprop="startDate" content="<?= $event->startDate() ?>" />
									<meta itemprop="endDate" content="<?= $event->endDate() ?>" />
									<?php else: ?>
									<time datetime="<?= $event->startDate() ?>"><?= strftime('%e %B %Y', strtotime($event->startDate())) ?></time>
									<meta itemprop="startDate" content="<?= $event->startDate() ?>" />
									<?php endif ?>
								</p>
							</header>
							<?php if ($event->link()->isNotEmpty()): ?>
							<p class="mono" aria-label="Lien"><a href="<?= $event->link() ?>" itemprop="url"><?= preg_replace( "#^[^:/.]*[:/]+#i", "", preg_replace( "{/$}", "", urldecode($event->link()) ) ) ?></a></p>
							<?php endif ?>
							<p class="mono" aria-label="Lieu" itemprop="location" itemscope itemtype="http://schema.org/Place">
								<?php if ($event->locality()->isNotEmpty()): ?>
								<meta itemprop="name" content="<?= $event->locality() ?>"><?php endif ?>
								<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
									<?php if ($event->locality()->isNotEmpty()): ?>
									<span itemprop="addressLocality"><?= $event->locality() ?></span>, <?php endif ?>
									<?php if ($event->department()->isNotEmpty()): ?>
									<?= $event->department() ?></span><?php endif ?>
									<?php if ($event->region()->isNotEmpty()): ?>
									<meta itemprop="addressRegion" content="<?= $event->region() ?>" /><?php endif ?>
									<?php if ($event->postalCode()->isNotEmpty()): ?>
									<meta itemprop="postalCode" content="<?= $event->postalCode() ?>" /><?php endif ?>
									<?php if ($event->country()->isNotEmpty()): ?>
									<meta itemprop="addressCountry" content="<?= $event->country() ?>" /><?php endif ?>
								</span>
							</p>
							<meta itemprop="description" content="<?= $event->description()->kti() ?>">
						</article>
						<?php endforeach ?>
					</section>

				</div>
			</main>

			<?php snippet('footer-page') ?>

		</div>
	</div>

	<?php snippet('footer') ?>
