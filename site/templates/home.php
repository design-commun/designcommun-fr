<?php snippet('header') ?>
	
	<div class="container min-h-full flex flex-col lg:flex-row">

		<div id="intro" class="flex-grow">
			<header id="header" class="min-h-14">
				<nav role="navigation" class="flex justify-between">
					<h1><?= $site->title() ?></h1>
				</nav>
			</header>

			<section class="text-2xl max-w-3xl">
				<?= $page->text()->kt() ?>
			</section>
		</div>

		<main class="">
			
			<div class="sections">
				<section id="events" class="section flex-1 flex-grow text-color-30-dark">
					<header class="relative flex justify-between">
						<h2><a class="link-full" href="<?= $events->url() ?>"><?= $events->title()->lower() ?></a></h2>
						<?php snippet('svg/calendar') ?>
					</header>
					
					<?php foreach ($events->children()->listed()->flip()->limit(1) as $event): ?>
					<article class="item event" itemscope itemtype="http://schema.org/Event">
						<header class="flex flex-col">
							<h3 class="mt-0"><a href="<?= $event->url() ?>" itemprop="name" class="border-b"><?= $event->title() ?></a></h3>
							<span aria-label="Date" class="item__date order-first">
							<?php if ($event->endDate()->isNotEmpty()): ?>
								<time datetime="<?= $event->startDate() ?>"><?= strftime('%e', strtotime($event->startDate())) ?></time><span aria-label="au">→</span><time datetime="<?= $event->endDate() ?>"><?= strftime('%e %B %Y', strtotime($event->endDate())) ?></time>
								<meta itemprop="startDate" content="<?= $event->startDate() ?>" />
								<meta itemprop="endDate" content="<?= $event->endDate() ?>" />
							<?php else: ?>
								<time datetime="<?= $event->startDate() ?>"><?= strftime('%e %B %Y', strtotime($event->startDate())) ?></time>
								<meta itemprop="startDate" content="<?= $event->startDate() ?>" />
							<?php endif ?>
							</span> <br>
						</header>
						<?php if ($event->link()->isNotEmpty()): ?>
						<p class="mono" aria-label="Lien"><a href="<?= $event->link() ?>" itemprop="url"><?= preg_replace( "#^[^:/.]*[:/]+#i", "", preg_replace( "{/$}", "", urldecode($event->link()) ) ) ?></a></p>
						<?php endif ?>
						<p class="mono" aria-label="Lieu" itemprop="location" itemscope itemtype="http://schema.org/Place">
							<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<?php if ($event->locality()->isNotEmpty()): ?>
								<span itemprop="addressLocality"><?= $event->locality() ?></span>, <?php endif ?>
								<?php if ($event->department()->isNotEmpty()): ?>
								<?= $event->department() ?></span><?php endif ?>
								<?php if ($event->region()->isNotEmpty()): ?>
								<meta itemprop="addressRegion" content="<?= $event->region() ?>" /><?php endif ?>
								<?php if ($event->postalCode()->isNotEmpty()): ?>
								<meta itemprop="postalCode" content="<?= $event->postalCode() ?>" /><?php endif ?>
								<?php if ($event->country()->isNotEmpty()): ?>
								<meta itemprop="addressCountry" content="<?= $event->country() ?>" /><?php endif ?>
							</span>
						</p>
						<?php if ($event->text()->isNotEmpty()): ?>
						<p class="item__description" itemprop="description"><?= $event->text()->kti() ?></p>
						<?php elseif ($event->description()->isNotEmpty()): ?>
						<p class="item__description" itemprop="description"><?= $event->description()->kti() ?></p>
						<?php endif ?>
					</article>
					<?php endforeach ?>
					<a href="<?= $events->url() ?>" class="btn">
						<span>Tous les événéments</span> 
						<svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M13.5 5L20 11.5M20 11.5H4M20 11.5L13.5 18" stroke="currentColor"/>
						</svg>
					</a>
				</section>

				<section id="cahiers" class="section text-color-180-dark max-h-14 relative">
					<header class="flex justify-between">
						<div>
							<?php $cahiers = page('cahiers') ?>
							<h2><a href="<?= $cahiers->url() ?>" class="link-full"><?= $cahiers->title()->lower() ?></a></h2>
						</div>
						<?php snippet('svg/page') ?>
					</header>
				</section>
				
				<!--
				<section id="ressources" class="section text-color-210-dark max-h-14">
					<header class="flex justify-between">
						<div>
							<h2>ressources</h2> <span class="mono disabled">(à venir)</span>
						</div>
						<svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M10 7H19V9H11H10V8V7ZM9 7H5V18H19V10H10H9V9V7ZM10 6H19H20V7V9V10V18V19H19H5H4V18V7V6H5H9H10Z" fill="currentColor"/>
						</svg>
					</header>
				</section>
				-->
			</div>

			<?= snippet('footer-page') ?>

		</main>

	</div>

	<?php snippet('footer') ?>
