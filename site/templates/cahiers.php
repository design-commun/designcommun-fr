<?php snippet('header') ?>

	<div class="container min-h-full flex flex-col lg:flex-row">
		
		<header id="header" class="w-full lg:w-12 min-h-14">
			<nav role="navigation" class="flex justify-between">
				<a href="<?= $site->homePage()->url() ?>" title="Retour"><span class="hidden lg:inline lg:fixed">←</span><span class="lg:hidden"><?= $site->title() ?></span></a>
			</nav>
		</header>
		
		<div class="relative flex-1 flex flex-col">
			<main class="flex-grow text-color-180-dark">
				
				<div class="sections">
					<section id="cahiers" class="section flex-1 text-color-180-dark">
						<header class="flex justify-between">
							<h1 class="text-base m-0"><?= $page->title()->lower() ?></h1>
							<?php snippet('svg/page') ?>
						</header>

						<p class="text-2xl max-w-3xl mb-14">
							<?= $page->text()->kti() ?>
						</p>
						
						<?php foreach ($page->children()->listed()->flip() as $cahier): ?>
						<article class="item mb-14">
							<header class="flex flex-col">
								<h2 class="m-0"><a href="<?= $cahier->url() ?>" class="border-b"><?= $cahier->title() ?></a></h2>
								<p title="Date de publication" class="item__date order-first">
									<time datetime="<?= $cahier->date() ?>"><?= strftime('%e %B %Y', strtotime($cahier->date())) ?></time>
								</p>
								<p class="mono"><span title="Auteur"><?= $cahier->author() ?></span></p>
							</header>
						</article>
						<?php endforeach ?>
					</section>

				</div>
			</main>

			<?php snippet('footer-page') ?>

		</div>
	</div>

	<?php snippet('footer') ?>
