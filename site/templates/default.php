<?php snippet('header') ?>

  <div class="container min-h-full flex flex-col lg:flex-row">
    
    <header id="header" class="w-full lg:w-12 min-h-14">
      <nav role="navigation" class="flex justify-between">
        <a href="<?= $site->homePage()->url() ?>" title="Retour"><span class="hidden lg:inline lg:fixed">←</span><span class="lg:hidden"><?= $site->title() ?></span></a>
      </nav>
    </header>
    
    <div class="relative flex-1 flex flex-col">

      <main class="flex-grow p-4 pb-16">
        
        <nav id="breadcrumb" class="text-base m-0 flex justify-between">
          <ol class="flex">
            <?php if ($page->parent()): ?>              
            <li><a href="<?= $page->parent()->url() ?>"><?= $page->parent()->title()->lower() ?></a></li>
            <?php endif ?>
            <li class="opacity-75"><?= $page->title()->lower() ?></li>
          </ol>
          <?php snippet('svg/page') ?>
        </nav>

        <article class="article flex flex-col lg:flex-row mg:lg:justify-between">

          <div class="flex-1">
            <header class="flex flex-col">
              <h1 class="m-0 text-5xl"><?= $page->title() ?></h1>
            </header>

            <div class="max-w-3xl markdown">
              <?php if ($page->intro()->isNotEmpty()): ?>
              <p class="text-2xl"><?= $page->intro()->kti() ?></p>                
              <?php endif ?>
              <?php if ($page->text()->isNotEmpty()): ?>
              <div class="mt-16"><?= $page->text()->kt() ?></div>
              <?php endif ?>
            </div>
            
            <div class="flex flex-wrap mt-12">
              <?php if ($page->hasDocuments()): ?>
              <?php $pdf = $page->documents()->filterBy('extension', 'pdf')->first() ?>
              <a href="<?= $pdf->url() ?>" class="btn" aria-labelledby="download">
                <svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M6.5 4.5h7.293L17.5 8.207V19.5h-11v-15z" stroke="currentColor"/>
                  <path d="M14 8.5h3.5v11h-11v-15h7v4h.5z" stroke="currentColor"/>
                </svg>
                <span id="download">Télécharger <small class="opacity-75">(PDF · <?= $pdf->niceSize() ?>)</small></span>
              </a>
              <?php endif ?>
              
              <?php if ($page->link()->isNotEmpty()): ?>
              <a href="<?= $page->link() ?>" class="btn">
                <span>Lire en ligne</span> 
                <svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M7 6.5H16.5M16.5 6.5L4.5 18.5M16.5 6.5V16" stroke="currentColor"/>
                </svg>
              </a>
              <?php endif ?>
              
              <?php if ($page->repository()->isNotEmpty()): ?>
              <a href="<?= $page->repository() ?>" class="btn">
                <span>Code source</span> 
                <svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M7 6.5H16.5M16.5 6.5L4.5 18.5M16.5 6.5V16" stroke="currentColor"/>
                </svg>
              </a>
              <?php endif ?>
            </div>
          </div>

        </article>

      </main>

      <?php snippet('footer-page') ?>

    </div>
  </div>

  <?php snippet('footer') ?>
