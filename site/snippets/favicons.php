<?php $path = 'assets/favicons/' ?>
<?php $version = '?v=PYAEdeNXJ1' ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?= url($path.'apple-touch-icon.png'.$version) ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?= url($path.'favicon-32x32.png'.$version) ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?= url($path.'favicon-16x16.png'.$version) ?>">
<link rel="manifest" href="<?= url('manifest.json') ?>">
<link rel="mask-icon" href="<?= url($path.'safari-pinned-tab.svg'.$version) ?>" color="#000000">
<link rel="icon" type="image/x-icon" href="<?= url($path.'favicon.ico'.$version) ?>">
<link rel="shortcut icon" href="<?= url('favicon.svg') ?>">
<meta name="msapplication-config" content="<?= url($path.'browserconfig.xml'.$version) ?>">
<meta name="theme-color" content="#ffffff">
