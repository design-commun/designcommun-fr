<footer role="contentinfo" class="flex">
	<p class="flex-grow"><?= $site->title() ?> <br>2020 · <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><abbr title="Attribution - Partage dans les Mêmes Conditions">CC BY-SA</abbr></a></p>
	<div class="flex justify-between">
		<div class="mr-8">
			<a rel="me" href="https://twitter.com/<?= $site->twitter() ?>">Twitter</a> <br>
			<a rel="me" href="<?= $site->mastodon() ?>">Mastodon</a>
		</div>
		<div>
			<span>contact<!-- --><span class="hidden">anti</span>&#64;<span class="hidden">spam</span>designcommun<!-- -->&#46;fr</span> <br>
			<?php $legal = page('mentions-legales') ?>
			<a href="<?= $legal->url() ?>"><?= $legal->title() ?></a>
		</div>
	</div>
</footer>
