<header id="header" class="h-24 py-5">
  
  <div class="flex items-center justify-between text-xl relative z-20" role="banner">
    <a id="logo-container" href="/" tabindex="-1">
      <svg id="logo" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 125.4 166.3" width="41.8" height="55.43" fill="currentColor" class="transition-transform ease-in-out-quint duration-700 hover:rotate-360">
        <circle cx="62.9" cy="83.1" r="21.3"/>
        <rect x="10" y="10" transform="matrix(0.9848 -0.1736 0.1736 0.9848 -11.5724 3.9993)" width="14.2" height="116.2"/>
        <rect x="40.7" y="102.1" transform="matrix(0.342 -0.9397 0.9397 0.342 -37.6232 164.7337)" width="116.2" height="14.2"/>
      </svg>
    </a>
    <div class="pl-4">
      <a href="https://www.hartpon-editions.com/" class="inline-block opacity-25 hover:opacity-100 mr-4">Éditions&nbsp;Hartpon</a>
      <a href="/" class="inline-block"><?= $site->title() ?></a>
    </div>
  </div>

  <input id="nav-toggle" type=checkbox class="hidden">
  <label id="show-button" for="nav-toggle" class="fixed top-0 left-0 z-40 p-6 py-10 flex items-center block md:hidden text-gray-500 hover:text-blue-500">
    <svg class="fill-current h-4 w-4" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <title>Ouvrir Menu</title>
      <path d="M0 3h20v2H0V3z m0 6h20v2H0V9z m0 6h20v2H0V0z" />
    </svg>
  </label>
  <label id="hide-button" for="nav-toggle" class="fixed top-0 left-0 z-40 p-6 py-10 flex items-center hidden text-gray-700 hover:text-blue-500">
    <svg class="fill-current h-4 w-4" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <title>Fermer Menu</title>
      <polygon points="11 9 22 9 22 11 11 11 11 22 9 22 9 11 -2 11 -2 9 9 9 9 -2 11 -2"
      transform="rotate(45 10 10)" />
    </svg>
  </label>
 
  <nav id="nav" class="list-none" role="navigation">
    <?php 
    // main menu items
    $items = $pages->listed();
    $i = 1;
    // only show the menu if items are available
    if ($items->isNotEmpty()): ?>
    <ul id="menu" aria-label="Menu" class="fixed z-20 top-0 left-0 transform origin-bottom-left">
      <?php foreach($items as $item): ?>
      <li class="relative bg-secondary w-full h-16 border-b py-3 pl-24 md:px-6 text-2xl transition-transform ease-in-out-quint duration-700<?= e($item->isOpen() || ($i == 1 && $page->isErrorPage()) || ($i == 1 && Str::contains($page->template(), "article")), ' active') ?>">
        <a class="inline-block" href="<?= $item->url() ?>"<?= e($item->isActive(), ' aria-current="page"') ?>><?= $item->title()->html() ?> </a>
      </li>
      <?php $i++ ?>
      <?php endforeach ?>
    </ul>
    <?php endif ?>
  </nav>
  
</header>
