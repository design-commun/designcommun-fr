<ul aria-label="Mots-clés" class="tags | list-separated-slash text-sm sm:text-base">
	<?php foreach ($data->tags()->split() as $tag): ?>
	<li class="tag"><?= $tag ?></li>
	<?php endforeach ?>
</ul>
