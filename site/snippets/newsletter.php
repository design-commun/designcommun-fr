<!-- AT Popup Beta 2021 BEGIN -->
<div id="_atPopupSU" class="shown">
  <div class="bl-template row-fluid bl-content-removable popup-dir-ltr" id="template-body" data-page-width="370" data-new-from-template="false">     
    <!-- BEGIN TEMPLATE OUTER -->     
    <div class="bl-template-main-wrapper span12" id="bl_0" valign="top">        
      <!-- BEGIN TEMPLATE MARGIN (Outside Margin - Padding) -->        
      <div class="bl-template-margin span12" id="bl_1" valign="top">            
        <!-- BEGIN TEMPLATE WRAPPER (Background) -->            
        <div class="template-main-table bl-template-background span12" id="bl_2" valign="top">                
          <!-- BEGIN TEMPLATE CONTAINER (Border, Inner Padding) -->                
          <div class="bl-template-border span12" id="bl_3" valign="top">                    
            <!-- BEGIN ZONES CONTAINER -->                    
            <!--zone-marked-->                    
            <div class="bl-zone bl-zone-dropable bl-zone-body row-fluid" id="bl_4" style="margin-top: 0px !important;" name="BodyZone" valign="top">         
              <div class="bl-block bl-block-signuptextpage" id="bl_5" blocktype="signuptextpage" name="signuptextpage" style="width: 370px;">
                <div class="bl-block-content" contenteditable="false"> 
                  <div> 
                    <div class="bl-block-content-table bl-block-dir-ltr span12"> 
                      <div class="bl-block-content-row bl-block-content-first-row bl-block-content-last-row span12"  data-bi=""> 
                        <div class="bl-block-content-row-inner span12">
                          <div class="bl-block-content-column bl-block-content-new-column span12">
                            <div class="bl-content-wrapper span12"> 
                              <div class="bl-signup-container block-align-left span12" at-form-width="12" style="border: 0px solid transparent;" data-bi="">   
                                <div class="bl-block-content-item bl-block-content-item-signupfieldpage bl-content-item-unremovable fields-left" style="text-align: center; margin-bottom: 0px;" data-is-auto-fill="true">
                                  <input type="text" maxlength="50" class="signup-field span12 first-input" readonly="readonly" data-field-type="email" data-field-source="Email" data-mandatory="true" placeholder="Votre email" data-field-validation-msg="La valeur saisie dans ce champ n’est pas valide." style="margin-bottom: 1em; height: 50px; line-height: 1.5; padding: 0.7em 0.75em 0.8em; box-shadow: none; border: 1px solid #000; border-radius: 0; font-family: 'Lora'" data-hidden="false" data-custom-values="" data-input-type="text" data-tag="undefined" id="sf-Email0"><div class="confirm-emails" data-field-validation-msg="Merci d’accepter de recevoir des emails."> 
                                    <div class="checkbox ltr"> 
                                      <label style="cursor: auto; margin-bottom: 1.5rem; font-family: 'Lora'; color: #000;"> 
                                        <input type="checkbox" disabled="disabled">
                                        <label class="confirm-label dir-label">J’accepte de recevoir des emails et je comprends que je peux me désabonner facilement à tout moment.</label>
                                      </label>
                                    </div> 
                                  </div>
                                  <!--
                                  <div class="hidden confirm-terms" data-field-validation-msg="Please agree to the terms and conditions" style="font-family: Arial;"> 
                                    <div class="checkbox ltr"> 
                                      <label style="cursor: auto;"> 
                                        <input type="checkbox" disabled="disabled">
                                        <label class="confirm-label dir-label" style="font-family: Arial; cursor: auto; font-size: 12px; color: #fff; text-align: left;">I agree to the terms and conditions</label>
                                      </label>
                                    </div> 
                                  </div>
                                  -->
                                </div> 
                                <div class="bl-padding-columns bl-content-wrapper-columns" style="text-align: center;"><div class="bl-block-button-content-wrapper" style="display: block; background: #000;"> 
                                  <div class="bl-block-button-content-item-wrapper"> 
                                    <div class="bl-block-content-item bl-block-content-item-button bl-content-item-unremovable" style="min-width: 160px; height: 50px; padding: 0.9em 1em; font-size: 16px; display: block; text-align: center; text-decoration: none; font-weight: bold; font-family: 'Lora'" data-gramm="false">
                                      <span style="color:#FFF;">Je m’abonne</span>
                                    </div> 
                                  </div> 
                                </div> 
                              </div> 
                            </div> 
                          </div>
                        </div>
                      </div> 
                    </div> 
                  </div> 
                </div> 
              </div>
            </div>                     
          </div>                    
          <!-- END ZONES CONTAINER -->                
        </div>                
        <!-- END TEMPLATE CONTAINER -->            
      </div>            
      <!-- END TEMPLATE WRAPPER -->        
    </div>        
    <!-- END TEMPLATE MARGIN -->    
  </div>    
  <!-- END TEMPLATE OUTER -->
</div>
</div>

<script type='text/javascript'>
  (function () {
    var _atpopq = window._atpopq || (window._atpopq = []);
    window._atpopobj = {};
    if (!_atpopq.loaded) {
      var v = Math.floor((Math.random() * 1000000) + 1);
      var atpopjs = document.createElement('script');
      atpopjs.type = 'text/javascript';
      atpopjs.async = true;
      atpopjs.src = '//cdn-media.web-view.net/popups/lib/v1/loader.min.js?v=' + v;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(atpopjs, s);
      _atpopq.loaded = true;
    }
    _atpopq.push(['UserId', 'zatfffduh222']);
    _atpopq.push(['PopupId', 'xwd3scpa']);
    _atpopq.push(['IsraelCode', '104']);
    _atpopq.push(['CountryCode', '74']);
    _atpopq.push(['IsEmbed', true]);
    _atpopq.push(['IgnoreMainCss', true]);
    _atpopq.push(['OnEventCallback', 'handleATPopupEvent']);
  })();
</script>
<script type="text/javascript">
  //Sample event handler function
  function handleATPopupEvent(ev,args){
    switch(ev){
      case 'display':
        //Do this when the popup is displayed
        break;
      case 'close':
        //Do this when the popup gets closed by the user
        break;
      case 'submit':
        //Do this when popup gets submitted and the user doesn't get redirected to a URL
        break;
    }
  }
</script>
<!-- AT Popup Beta END -->