<nav class="prev-next | flex justify-between py-3 mb-3 -mx-4">

	<a href="<?= $page->parent()->url() ?>" class="inline-block w-12 h-12 flex items-center justify-center opacity-50 hover:opacity-100" title="<?= $page->template() == "article" ? "Retour aux articles" : "Retours aux projets" ?>">
		<svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor"aria-hidden="true" >
			<path d="M2 6.5L14 6.5" />
			<path d="M7 1L1.5 6.5L7 12" />
		</svg></a>

	<div class="flex relative h-12">
		<div class="prev">
			<?php if ($page->hasPrevListed()): ?>
			<a rel="prev" class="inline-block w-12 h-12 p-3 opacity-50 hover:opacity-100" href="<?= $page->prevListed()->url() ?>" title="<?= $prev ?>">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
					<path d="M13.5 6L8 11.5L13.5 17" />
				</svg>
			</a>
			<?php else: ?>
			<div class="w-12 h-12 p-3 opacity-25 cursor-not-allowed" aria-hidden="true">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
					<path d="M13.5 6L8 11.5L13.5 17" />
				</svg>
			</div>
			<?php endif ?>
		</div>
		<div class="next">
			<?php if ($page->hasNextListed()): ?>
			<a rel="next" class="inline-block w-12 h-12 p-3 opacity-50 hover:opacity-100" href="<?= $page->nextListed()->url() ?>" title="<?= $next ?>">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
				  <path d="M10 6L15.5 11.5L10 17" />
				</svg>
			</a>
			<?php else: ?>
			<div class="w-12 h-12 p-3 opacity-25 cursor-not-allowed" aria-hidden="true">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
				  <path d="M10 6L15.5 11.5L10 17" />
				</svg>
			</div>
			<?php endif ?>
		</div>
	</div>
</nav>
