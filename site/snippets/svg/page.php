<svg aria-hidden="true" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" xmlns="http://www.w3.org/2000/svg">
	<path d="M6.5 4.5h7.293L17.5 8.207V19.5h-11v-15z"/>
	<path d="M14 8.5h3.5v11h-11v-15h7v4h.5z"/>
</svg>