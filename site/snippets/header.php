<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="generator" content="Kirby (<?= Kirby::version() ?>)">
  
  <?php 
  $template = $page->template(); 
  setlocale(LC_ALL, 'fr_FR.utf8', 'fra');
  ?>

  <?= $page->metaTags() ?>
  <?= snippet('schema/website') ?>
  <?= snippet('schema/organization') ?>
  <?= snippet('schema/breadcrumb') ?>
  <?= snippet('favicons') ?>

  <?= css([
    'assets/css/main.css?'.filemtime('assets/css/main.css'),
    '@auto'
  ]) ?>

</head>

<body id="<?= $template ?>">
