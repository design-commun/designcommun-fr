<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "name": "<?= $site->title() ?>",
    "alternateName": "Design Commun",
    "url": "<?= $site->url() ?>",
    "description": "<?= $site->description() ?>",
    "inLanguage": "fr",
    "sameAs": [
      "https://twitter.com/designcommun",
      "https://mastodon.design/@designcommun"
    ]
  }
</script>
