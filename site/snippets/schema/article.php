<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Article",
    "author":{
      "name": "<?= $site->title() ?>"
    },
    "headline": "<?= $page->title() ?>",
    "description": "<?= $page->description()->isNotEmpty()
      ? $page->description()
      : ($page->introduction()->isNotEmpty() ? $page->introduction()->kti() : $site->description() ) ?>",
    "inLanguage": "fr",
    "wordCount": "<?= $page->text()->words() ?>",
    "datePublished": "<?= $page->date() ?>",
    "dateModified": "<?= $page->modified('Y-m-d') ?>",
    <?php if ($page->cover()->isNotEmpty()): ?>
    "image": "<?= $page->cover()->toFiles()->first()->url() ?>",        
    <?php endif ?>
    "publisher": {
      "@type": "Organization",
      "name": "<?= $site->title() ?>",
      "logo":{
        "@type": "ImageObject",
        "url": "https://designcommun.fr/assets/favicons/android-chrome-512x512.png",
        "height": 512,
        "width": 512
      }
    }
  }
</script>
