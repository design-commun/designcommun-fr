<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "item": {
        "@id": "<?= $site->homePage()->url() ?>",
        "name": "<?= $site->title() ?>"
      }
    }
    <?php if ($page->template() == "articles" || $page->template() == "projets"): ?>,
    {
      "@type": "ListItem",
      "position": 2,
      "item": 
      {
        "@id": "<?= $page->url() ?>",
        "name": "<?= $page->title() ?>"
      }
    }
    <?php endif ?>
    <?php if ($page->template() == "article" || $page->template() == "projet"): ?>,
    {
      "@type": "ListItem",
      "position": 2,
      "item": 
      {
        "@id": "<?= $page->parent()->url() ?>",
        "name": "<?= $page->parent()->title() ?>"
      }
    },
    {
      "@type": "ListItem",
      "position": 3,
      "item": 
      {
        "@id": "<?= $page->url() ?>",
        "name": "<?= $page->title() ?>"
      }
    }
    <?php endif ?>
    ]
  }
</script>
