<?php

return [
  'url' => 'https://designcommun.fr',
  'debug' => true,
  'locale' => 'fr_FR.utf8',
  'smartypants' => true,
  'markdown' => [
    'extra' => true
  ],
  'cache' => [
    'pages' => [
      'active' => true,
      // 'ignore' => function ($page) {
      //   return $page->title()->value() === 'Do not cache me';
      // }
    ],
  ],
  'thumbs' => [
    'srcsets' => [
      'default' => [
        '300w'  => ['width' => 300, 'quality' => 75],
        '600w'  => ['width' => 600, 'quality' => 75],
        '1200w' => ['width' => 1200, 'quality' => 75]
      ]
    ]
  ],
  'bvdputte.flocoff.enabled' => true,
  'routes' => [
    // https://github.com/bnomei/kirby3-feed
    [
      'pattern' => 'feed',
      'method' => 'GET',
      'action'  => function () {
        $options = [
          'url'         => site()->url(),
          'title'       => 'Dernières actualités de '.site()->title(),
          'description' => site()->description(),
          'link'        => site()->url(),
          'urlfield' => 'url',
          'titlefield' => 'title',
          'datefield' => 'date',
          'textfield' => 'text',
          'modified' => time(),
          'snippet' => 'feed/rss', // 'feed/json'
          'mime' => null,
          'sort' => true,
        ];
        $feed = collection('actualites')->limit(10)->feed($options);
        return $feed;
      }
    ]
  ],
  'community.markdown-field' => [
    'size'      => 'small',
    'buttons'   => ['headlines', 'bold', 'italic', 'divider', 'link', 'email', 'file', 'code', 'divider', 'ul', 'ol'],
    'font'      => [
      'family'  => 'sans-serif',
      'scaling' => false,
      'size'    => 'regular',
    ],
    'query'      => [
      'pagelink' => null,
    ],
    'modals'     => true,
    'blank'      => false,
    'invisibles' => true,
    'direction' => false,
  ],
  // https://github.com/omz13/kirby3-xmlsitemap
  'omz13.xmlsitemap' => [
    'cacheTTL' => 60,
    'includeUnlistedWhenSlugIs' => [ ],
    'includeUnlistedWhenTemplateIs' => [ ],
    'excludePageWhenTemplateIs' => [ ],
    'excludePageWhenSlugIs' => [ ],
    'excludeChildrenWhenTemplateIs' => [ ],
    'disableImages' => false
  ],
  // https://github.com/omz13/kirby3-wellknown
  'omz13.wellknown' => [
    'disable' => false,
    'notfound' => true,
    'not-txt-notfound' => true,
    'x-ping' => true
  ],
  // https://github.com/pedroborges/kirby-meta-tags
  'pedroborges.meta-tags.default' => function ($page, $site) {
    $title = $page->isHomePage()
      ? $site->title()
      : ($page->template() == "cahier" ? $page->author().", ".$page->title() : $page->title())." | ".$site->title();
    $description = $page->description()->isNotEmpty()
      ? $page->description()
      : ($page->description()->isNotEmpty() ? $page->description()->kti() : $site->description() );
    return [
      'title' => $title,
      'meta' => [
        'description' => $description,
        'robots' => 'index,follow,noodp' /* noindex,nofollow */
      ],
      'link' => [
        'canonical' => $page->url(),
        'image_src' => function ($page) {
          if ($page->isHomePage() || !$page->hasImages()) {
            $image = new Asset('assets/img/thumbnail.png');
          }
          else {
            $image = $page->image('thumbnail.png');
          }
          return $image->url();
        }
      ],
      'og' => [
        'title' => $title,
        'description' => $description,
        'type' => 'website',
        'site_name' => $site->title(),
        'url' => $page->url(),
        'locale' => 'fr',
        'namespace:image' => function ($page, $site) {
          if ($page->isHomePage() || !$page->hasImages()) {
            $image = new Asset('assets/img/thumbnail.png');
            $alt = $site->title();
          }
          else {
            $image = $page->image('thumbnail.png');
            $alt = $image->alt()->isNotEmpty() 
            ? $image->alt() 
            : $image->title();
          }
          return [
            'image' => $image->url(),
            'type' => $image->mime(),
            'width' => $page->isHomePage() || !$page->hasImages() ? '1200' : $image->width(),
            'height' => $page->isHomePage() || !$page->hasImages() ? '628' : $image->height(),
            'alt' => $alt
          ];
        }
      ],
      'twitter' => [
        'card' => 'summary',
        'title' => $title,
        'site' => $site->twitter(),
        'description' => $description,
        'namespace:image' => function ($page, $site) {
          if ($page->isHomePage() || !$page->hasImages()) {
            $image = new Asset('content/site.png');
            $alt = $site->title();
          }
          else {
            $image = $page->image('thumbnail.png');
            $alt = $image->alt()->isNotEmpty() 
            ? $image->alt() 
            : $image->title();
          }
          return [
            'image' => $image->url(),
            'alt' => $alt
          ];
        }
      ]
    ];
  }
];