<?php
class FichePage extends Page {
  public function cover() {
    if ($this->hasImages()) {
    	return $this->cover()->isNotEmpty() ? $this->cover()->toFile() : $this->images()->sortBy('order')->first();
    }
    else {
    	return null;
    }
  }
}