<?php

return function ($site) {
	return page('events')->children()->listed()->flip();
};
