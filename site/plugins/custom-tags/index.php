<?php

$originalTag = Kirby\Text\KirbyTag::$types['image'];
Kirby::plugin('timotheegoguely/custom-tags', [
    'tags' => [
        'image' => [
            'attr' => $originalTag['attr'],
            'html' => function($tag) use ($originalTag)  {

                $file = $tag->file($tag->value());
                $result = $originalTag['html']($tag);

                if (! $file === true) {
                    return $result;
                }

                $pattern = '/<img.*?>/i';

                // build a new image tag with the srcset
                $image = Html::img($tag->src, [
                    'width'  => $tag->width,
                    'height' => $tag->height,
                    'class'  => $tag->imgclass,
                    'title'  => $tag->title,
                    'alt'    => $tag->alt ?? ' ',
                    'srcset' => $file->srcset(),
                    'loading' => 'lazy'
                ]);

                // replace the old image tag
                $result = preg_replace($pattern, $image, $result);

                return $result;
            }
        ]
    ]
]);